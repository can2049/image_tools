#!/usr/bin/env python3
# tested on linux

import sys
import time
import os
from datetime import datetime
import re
import argparse
import multiprocessing
import shutil


def check_exiftool_installed():
    return os.system("which exiftool > /dev/null") == 0


def is_valid_date_time(date_string):
    # with format "YYYYMMDD_HHMMSS"
    try:
        # Split the string into date and time parts
        date_part, time_part = date_string.split("_")

        # Parse the date and time
        dt_obj = datetime.strptime(date_part + time_part, "%Y%m%d%H%M%S")

        # Check if the parsed datetime object is valid
        if (
            1 <= dt_obj.month <= 12
            and 1 <= dt_obj.day <= 31
            and 0 <= dt_obj.hour <= 23
            and 0 <= dt_obj.minute <= 59
            and 0 <= dt_obj.second <= 59
        ):
            return True
        else:
            return False

    except ValueError:
        return False


def image_path_has_date(image_path):
    """
    Check if the given image path contains a date in the format "YYYYMMDD_HHMMSS".

    Parameters:
    image_path (str): The path of the image file.

    Returns:
    bool: True if the image path contains a date in the specified format, False otherwise.
    """
    date_pattern = r"\d{8}_\d{6}"  # Pattern to match "YYYYMMDD_HHMMSS"
    match = re.search(date_pattern, image_path)
    if match is not None:
        # get the matched part
        date_string = match.group()
        if is_valid_date_time(date_string):
            return True
    # print(
    #     f"image path does not contain a date in the format 'YYYYMMDD_HHMMSS', or date/time is not right. {image_path}"
    # )
    return False


def check_exiftool_version():
    if not check_exiftool_installed():
        print(
            "exiftool not installed. use the cmd to install it:\n sudo apt install exiftool"
        )
        sys.exit(1)
    version_output = os.popen("exiftool -ver").read()
    version = version_output.strip()
    # compare version, at least 12.40
    least_version = "12.40"
    if version < least_version:
        print(f"exiftool version: {version}, should newer than {least_version} ")


def get_create_date_of_image(image_path):
    # Execute exiftool to retrieve and format the create date of the image, with format "YYYYMMDD_HHMMSS"
    query_output = (
        os.popen(f"exiftool -CreateDate -d '%Y%m%d_%H%M%S' '{image_path}'")
        .read()
        .strip()
    )

    # Return an empty string if there's no output from the exiftool command
    if not query_output:
        # print(f"No create date found for {image_path}")
        return None

    # Parse and return the creation date from the query output
    return query_output.split(" : ")[1].strip()


def get_file_extension(filename):
    """
    '/path/to/document.txt' => '.txt'.
    """
    return os.path.splitext(filename)[1]


def get_file_stem_name(filename):
    """
    '/path/to/document.txt' => 'document'.
    """
    return os.path.splitext(filename)[0]


def parse_args():
    parser = argparse.ArgumentParser(
        description="Rename images/videos based on their creation dates. powered by exiftool"
    )
    parser.add_argument(
        "image_dir",
        type=str,
        help="The directory containing the images and videos to read.",
    )

    # Option to rename files directly in their current location
    parser.add_argument(
        "--rename-inplace",
        action="store_true",
        help="Rename the files in their original location. If this flag is set, do not specify a save directory. Prefer to use --save-dir instead.",
    )

    # Option to provide a directory to save renamed files
    parser.add_argument(
        "--save-dir",
        type=str,
        help="A directory to save renamed files. If specified, the original files will remain unchanged. Cannot be used simultaneously with --rename-inplace.",
    )

    args = parser.parse_args()

    # Validate argument combinations
    if not args.rename_inplace and not args.save_dir:
        parser.error(
            "At least one of the options '--rename-inplace' or '--save-dir' must be provided."
        )

    if args.rename_inplace and args.save_dir:
        parser.error(
            "The '--rename-inplace' and '--save-dir' options are mutually exclusive; only one can be used at a time."
        )

    return args


g_modify_count = multiprocessing.Value("i", 0)
g_lock = multiprocessing.Lock()


def process_image_video(root, file, save_dir=None):
    # print(f"begin to process file {file}")
    image_path = os.path.join(root, file)
    create_date = get_create_date_of_image(image_path)
    if create_date is None:
        return

    extension = get_file_extension(file)
    stem_name = get_file_stem_name(file)
    new_filename = f"{stem_name}_{create_date}{extension}"

    modified = False
    if save_dir is not None:
        new_file_path = os.path.join(save_dir, new_filename)
        shutil.copy2(image_path, new_file_path)
        print(f"{image_path} -> {new_file_path}")
        modified = True
    else:
        new_file_path = os.path.join(root, new_filename)
        os.rename(image_path, new_file_path)
        print(f"{image_path} -> {new_file_path}")
        modified = True
    if modified:
        with g_lock:
            g_modify_count.value += 1


def rename_image_video_with_create_date_parallel():
    args = parse_args()
    print("args: ", args)
    image_dir = args.image_dir
    if not os.path.exists(image_dir):
        print(f"{image_dir} does not exist, exit")
        return

    check_exiftool_version()
    if args.save_dir is not None:
        save_dir = args.save_dir
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

    start_time = time.time()

    pool = multiprocessing.Pool(processes=multiprocessing.cpu_count() - 1)

    for root, dirs, files in os.walk(image_dir):
        for file in files:
            if file.lower().endswith((".jpg", ".jpeg", ".heic", ".mov")):
                if image_path_has_date(file):
                    continue

                pool.apply_async(
                    process_image_video,
                    args=(root, file, args.save_dir),
                )

    pool.close()
    pool.join()

    end_time = time.time()
    execution_time = end_time - start_time
    print(
        f"Modified {g_modify_count.value} files. Total execution time: {execution_time:.2f} seconds"
    )


if __name__ == "__main__":
    rename_image_video_with_create_date_parallel()
